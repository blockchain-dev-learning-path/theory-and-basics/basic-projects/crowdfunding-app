// SPDX-License-Identifier: MIT
pragma solidity ^0.8.18;


import { Project } from "./Project.sol";

contract ProjectFactory {

    Project[] public projects;

    function createProject(address _funderRegistryAddress, string memory _projectName) public {
        Project newProject = new Project(msg.sender, _funderRegistryAddress, _projectName);

        projects.push(newProject);
    }

    function getProjectsCreated() public view returns(Project[] memory) {
        return projects;
    }

}