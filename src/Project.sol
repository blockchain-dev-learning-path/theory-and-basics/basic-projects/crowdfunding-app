// SPDX-License-Identifier: MIT
pragma solidity ^0.8.18;

import {FunderRegistry} from "./FunderRegistry.sol";

contract Project {

    FunderRegistry public funderRegistry;

    address private immutable i_owner;

    uint256 public totalAmountRaised;

    uint256 public currentAmountRemaining;

    string public projectName;

    bool public isFundingComplete;

    mapping (address funderAddress => uint256 amountFunded) private projectFundersXAmountFunded;


    constructor(address projectOwner, address _funderRegistryAddress, string memory _projectName) {
        funderRegistry = FunderRegistry(_funderRegistryAddress);
        totalAmountRaised = 0;
        i_owner = projectOwner;
        projectName = _projectName;
        isFundingComplete = false;
    }

    // Modifiers:

    modifier onlyOwner() {
        require(msg.sender == i_owner, "Only the project owner can perform this transaction.");
        _;
    }

    modifier onlyRegisteredFunders() {
        // Check that the msg.sender is a Registered Funder
        FunderRegistry.Funder memory funder = funderRegistry.getFunder(msg.sender);
        require(funder.funderAddress != address(0), "Only registered funders can perform this action.");
        _;
    }

    modifier fundingIsActive() {
        require(isFundingComplete == false, "This project is currently not active for funding.");
        _;
    }


    // Getters:

    function getOwner() public view returns(address) {
        return i_owner;
    }

    function getAddressToAmountFunded(address fundingAddress) public view returns(uint256) {
        return projectFundersXAmountFunded[fundingAddress];
    } 

    function getProjectName() public view returns(string memory) {
        return projectName;
    }

    function getIsFundingComplete() public view returns(bool) {
        return isFundingComplete;
    }


    // Setters:

    function setProjectName(string memory _newName) public onlyOwner {
        projectName = _newName;
    }


    // Functions:

    function isFunder(address _potentialFunder) public view returns(bool) {
        bool isProjectFunder = false;

        if (projectFundersXAmountFunded[_potentialFunder] > 0) {
            isProjectFunder = true;
        }

        return isProjectFunder;
    }

    function fundProject() public payable onlyRegisteredFunders fundingIsActive {
        require(msg.value > 0, "Contribution value must be greater than 0.");

        // Update the funder's data
        // 1. Check if Funder didn't fund this project yet and add it as another project funded.
        if(!isFunder(msg.sender)) {
            // 1.b. If didn't fund it yet, We add this Project to the array of the projects he funded and his totalAmountFunded
            funderRegistry.addNewProjectFunded(msg.sender, address(this));

        }

        // 2. Update the funder's totalAmountFunded and amountFunded for this project in the projectFundersXAmountFunded mapping
        funderRegistry.updateFunderTotalAmountFunded(msg.sender, msg.value);
        projectFundersXAmountFunded[msg.sender] += msg.value;

        // 3. Update this Project's totalAmountRaised:
        totalAmountRaised += msg.value;
        currentAmountRemaining += msg.value;
    }


    function switchFundingIsComplete() public onlyOwner {
        isFundingComplete = !isFundingComplete;
    }


    function withdraw() public onlyOwner {
        require(currentAmountRemaining > 0, "There are currently no funds to withdraw.");
        currentAmountRemaining = 0;
        payable(i_owner).transfer(address(this).balance);
    }
}