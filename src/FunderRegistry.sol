// SPDX-License-Identifier: MIT
pragma solidity ^0.8.18;


contract FunderRegistry {
    struct Funder {
        string name;
        address funderAddress;
        string email;
        uint256 totalAmountFunded;
        address[] projectsFunded;
    }

    mapping (address => Funder) public funders;

    function registerAsFunder(string memory _name, string memory _email) public {
        Funder storage funder = funders[msg.sender];
        require(funder.funderAddress == address(0), "Funder already registered with this address.");
        funder.name = _name;
        funder.funderAddress = msg.sender;
        funder.email = _email;
    }

    function getFunder(address funderAddress) public view returns(Funder memory) {
        return funders[funderAddress];
    }

    function updateFunderInfo(address _funder, string memory _name, string memory _email) public {
        Funder storage funder = funders[_funder];

        funder.name = _name;
        funder.email = _email;

    }

    function addNewProjectFunded(address _funder, address _newProjectFunded) public {
        Funder storage funder = funders[_funder];
        funder.projectsFunded.push(_newProjectFunded);
    }

    function updateFunderTotalAmountFunded(address _funder, uint256 _amountAdded) public {
        Funder storage funder = funders[_funder];
        funder.totalAmountFunded += _amountAdded;
    }

    
}